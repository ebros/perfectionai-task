const dev = process.env.NODE_ENV !== 'production'
const path = require('path')
const nodeExternals = require('webpack-node-externals')


const config = {
	target: 'async-node',
	mode: dev ? 'development' : 'production',
	devtool: dev ? 'cheap-source-map' : 'source-map',
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.js',
	},
	stats: 'minimal',
	node: {
		__dirname: false,
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				options: {
					presets: [['@babel/preset-env', { 'targets': { 'node': 'current' } }]],
					plugins: [
						'@babel/plugin-transform-runtime',
					],
				},
			},
		],
	},
	externals: [
		nodeExternals(),
	],
}


module.exports = config
