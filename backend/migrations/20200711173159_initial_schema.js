
exports.up = async function (knex) {
	await Promise.all([
		knex.schema.createTable('wallets', table => {
			table.string('currency', 8).notNullable().primary()
			table.decimal('balance', 18, 9).notNullable()
		}),
		knex.schema.createTable('operations', table => {
			table.increments('id').notNullable().primary()
			table.string('fromCurrency', 8).notNullable()
			table.decimal('fromAmount', 18, 9).notNullable()
			table.string('toCurrency', 8).notNullable()
			table.decimal('toAmount', 18, 9).notNullable()
			table.decimal('fee', 18, 9).notNullable()
			table.datetime('time').notNullable()
		}),
		knex.schema.createTable('currencies', table => {
			table.string('symbol', 8).notNullable().primary()
			table.decimal('price', 18, 9).notNullable()
		}),
	]).catch(async error => {
		await exports.down(knex)
		throw error
	})
}


exports.down = async function (knex) {
	await Promise.all([
		knex.schema.dropTable('wallets'),
		knex.schema.dropTable('operations'),
		knex.schema.dropTable('currencies'),
	])
}
