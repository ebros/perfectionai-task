const config = require('./config')
const connection = require('url').parse(config.mysqlConnectionString)

module.exports = {
	client: 'mysql',
	connection: {
		host: connection.hostname,
		port: connection.port || 3306,
		user: connection.auth.split(':')[0],
		password: connection.auth.split(':').slice(1).join(':'),
		database: connection.path.substr(1),
		supportBigNumbers: true,
		bigNumberStrings: true,
	},
}
