import express, { Router } from 'express'
import wrap from 'express-async-wrap'

import config from '../config'
import Wallet from './models/Wallet'
import Currency from './models/Currency'
import Operation from './models/Operation'
import { ValidationError, validateJSON } from './util'
import { convert } from './services/conversionService'
import BigNumber from 'bignumber.js'


const currencies = [...config.cryptoCurrencies, ...config.fiatCurrencies]

const router = new Router()

router.use(express.json())


router.get('/init', wrap(async (req, res) => {

	await Wallet.transaction(async trx => {

		await Wallet.query(trx).delete()
		await Operation.query(trx).delete()

		for (const currency of currencies) {
			await Wallet.query(trx).insert({
				currency,
				balance: 1000,
			})
			await Operation.query(trx).insert({
				fromCurrency: 'USD',
				fromAmount: 0,
				toCurrency: currency,
				toAmount: 1000,
				fee: 0,
				time: new Date(),
			})
		}
	})

	await Currency.updateCache()

	res.sendStatus(201)
}))


router.get('/balances', wrap(async (req, res) => {
	const balances = await Wallet.query()
	res.json(balances.map(x => ({
		currency: x.currency,
		balance: BigNumber(x.balance),
	})))
}))


router.get('/operations', wrap(async (req, res) => {
	const size = Math.min(Math.max((req.query.size | 0) || 10, 1), 100)
	const page = (req.query.page | 0) || 0

	if (req && res && req.headers['return-total-count'] === 'true') {
		res.set('Total-Count', await Operation.countDocuments({}))
	}

	const operations = await Operation.query()
		.orderBy('id', 'desc')
		.limit(size).offset(page * size)

	res.json(operations.map(x => ({
		fromCurrency: x.fromCurrency,
		fromAmount: BigNumber(x.fromAmount),
		toCurrency: x.toCurrency,
		toAmount: BigNumber(x.toAmount),
		fee: BigNumber(x.fee),
		time: x.time,
	})))
}))


router.post('/sell', wrap(async (req, res) => {
	validateJSON(req.body, {
		type: 'object', required: true, properties: {
			from: { type: 'string', required: true },
			amount: { type: ['string', 'number'], required: true, min: 0 },
			to: { type: 'string', required: true },
		},
	})

	const fromAmount = BigNumber(req.body.amount).decimalPlaces(Currency.decimalPlaces(req.body.from))
	if (!fromAmount.isGreaterThan(0)) {
		throw new ValidationError('amount must be positive number')
	}

	const { rate, amount, fee, total } = await convert({
		from: req.body.from,
		fromAmount: BigNumber(req.body.amount),
		to: req.body.to,
	})

	res.json({
		rate,
		amount,
		amountCurrency: req.body.to,
		fee,
		feeCurrency: req.body.from,
		total,
		totalCurrency: req.body.from,
	})
}))


router.post('/buy', wrap(async (req, res) => {
	validateJSON(req.body, {
		type: 'object', required: true, properties: {
			from: { type: 'string', required: true },
			amount: { type: ['string', 'number'], required: true, min: 0 },
			to: { type: 'string', required: true },
		},
	})

	const toAmount = BigNumber(req.body.amount).decimalPlaces(Currency.decimalPlaces(req.body.to))
	if (!toAmount.isGreaterThan(0)) {
		throw new ValidationError('amount must be positive number')
	}

	const { rate, amount, fee, total } = await convert({
		from: req.body.from,
		to: req.body.to,
		toAmount,
	})

	res.json({
		rate,
		amount,
		amountCurrency: req.body.to,
		fee,
		feeCurrency: req.body.from,
		total,
		totalCurrency: req.body.from,
	})
}))


router.post('/sell-all', wrap(async (req, res) => {
	validateJSON(req.body, {
		type: 'object', required: true, properties: {
			from: { type: 'string', required: true },
			to: { type: 'string', required: true },
		},
	})

	const wallet = await Wallet.query().where({ currency: req.body.from }).first()
	if (!wallet) {
		throw new ValidationError(`Invalid currency "${req.body.from}"`)
	}
	if (BigNumber(wallet.balance).isZero()) {
		throw new ValidationError(`insufficient balance "${req.body.from}"`)
	}

	const { rate, amount, fee, total } = await convert({
		from: req.body.from,
		fromAmount: BigNumber(wallet.balance),
		withFee: true,
		to: req.body.to,
	})

	res.json({
		rate,
		amount,
		amountCurrency: req.body.to,
		fee,
		feeCurrency: req.body.from,
		total,
		totalCurrency: req.body.from,
	})
}))


router.use((error, req, res, next) => { // eslint-disable-line
	if (error.statusText) {
		console.error(error.statusText + ' ' + error.url)
		return res.sendStatus(500)
	}
	if (error.message === 'ValidationError') {
		console.warn(error)
		return res
			.status(400)
			.json({ message: error.message, extra: error.extra })
	}
	if (error.stack && error.stack.includes('JSON.parse')) {
		console.warn(error)
		return res
			.status(400)
			.json({ message: error.message })
	}

	console.error(error)
	return res.sendStatus(500)
})

export default router


