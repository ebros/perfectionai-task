import config from '../../config'
import BigNumber from 'bignumber.js'
const { Model } = require('objection')


export default class Currency extends Model {

	static get tableName() {
		return 'currencies'
	}

	static get decimalPlacesBySymbol() {
		return {
			USD: 2,
			EUR: 2,
			VNDC: 0,
			ETH: 9,
			BTC: 8,
		}
	}

	static decimalPlaces(symbol) {
		return Currency.decimalPlacesBySymbol[symbol] || 0
	}

	static async getPrice(symbol) {
		const fetch = require('node-fetch')
		const url = `https://pro-api.coinmarketcap.com/v1/tools/price-conversion?amount=1&symbol=USD&convert=${symbol}`
		const data = await fetch(url, {
			headers: { 'X-CMC_PRO_API_KEY': config.CMCApiKey },
		}).then(res => {
			if (!res.ok) { throw res }
			return res.json()
		})

		return { symbol, price: BigNumber(1).dividedBy(BigNumber(data.data.quote[symbol].price)).toString() }
	}

	static async updateCache () {
		const currencies = [...config.cryptoCurrencies, ...config.fiatCurrencies]

		const rates = await Promise.all(
			currencies.filter(x => x !== 'USD').map(Currency.getPrice),
		)

		await Currency.transaction(async trx => {
			await Currency.query(trx).delete()
			await Currency.query(trx).insert({
				symbol: 'USD',
				price: 1,
			})

			for (const rate of rates) {
				await Currency.query(trx).insert({
					symbol: rate.symbol,
					price: rate.price,
				})
			}
		})
	}
}
