const { Model } = require('objection')

class Operation extends Model {

	static get tableName() {
		return 'operations'
	}
}

module.exports = Operation
