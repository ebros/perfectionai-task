import { CronJob } from 'cron'

import Currency from './models/Currency'


function start() {
	scheduleJob('*/1 * * * *', 'update currency cache', async () => {
		await Currency.updateCache()
	})
}


function scheduleJob(schedule, title, func) {
	const job = new CronJob(schedule, function () {
		const maybePromise = func()
		if (maybePromise) {
			maybePromise.catch(error => {
				if (error) {
					console.error(`Cron job "${title}" error`, { error })
				}
			})
		}
	}, null, true)

	return () => job.stop()
}


export default {
	start,
}

