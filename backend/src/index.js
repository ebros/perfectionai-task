import app, { knex } from './app'

import config from '../config'
import services from './services'


app.listen(config.httpPort, () => {
	console.info(`listening on port ${config.httpPort}`)
})


services.start()


class WebpackMigrationSource {
	constructor(migrationContext) {
		this.migrationContext = migrationContext
	}

	getMigrations() {
		return Promise.resolve(this.migrationContext.keys().sort())
	}

	getMigrationName(migration) {
		return migration
	}

	getMigration(migration) {
		return this.migrationContext(migration)
	}
}

const migrationSource = new WebpackMigrationSource(require.context('../migrations', false, /.js$/))

knex.migrate.list({ migrationSource }).then(([, pending]) => {
	if (pending.length > 0) {
		knex.migrate.latest({ migrationSource }).catch(console.error)
	}
})

