import assert from 'assert'

import config from '../../config'
import Wallet from '../models/Wallet'
import Currency from '../models/Currency'
import Operation from '../models/Operation'
import { ValidationError } from '../util'
import BigNumber from 'bignumber.js'


export async function convert({ from, to, fromAmount, toAmount, withFee }) {
	assert(fromAmount || toAmount, 'fromAmount or toAmount is required')

	const [fromCurrency, toCurrency] = await Promise.all([
		Currency.query().where({ symbol: from }).first(),
		Currency.query().where({ symbol: to }).first(),
	])
	if (!fromCurrency) {
		throw new ValidationError(`Invalid currency "${from}"`)
	}
	if (!toCurrency) {
		throw new ValidationError(`Invalid currency "${to}"`)
	}

	const rate = BigNumber(fromCurrency.price).dividedBy(BigNumber(toCurrency.price)).decimalPlaces(8)
	if (toAmount) {
		fromAmount = toAmount.dividedBy(rate).decimalPlaces(Currency.decimalPlaces(from), BigNumber.ROUND_UP)
	}

	let amount, fee, total
	if (withFee) {
		total = fromAmount
		fee = total.multipliedBy(config.conversionFee).decimalPlaces(Currency.decimalPlaces(from), BigNumber.ROUND_UP)
		amount = total.minus(fee).multipliedBy(rate).decimalPlaces(Currency.decimalPlaces(to), BigNumber.ROUND_DOWN)
	}
	else {
		amount = fromAmount.multipliedBy(rate).decimalPlaces(Currency.decimalPlaces(to), BigNumber.ROUND_DOWN)
		fee = fromAmount.multipliedBy(config.conversionFee).decimalPlaces(Currency.decimalPlaces(from), BigNumber.ROUND_UP)
		total = fromAmount.plus(fee)
	}

	await Wallet.transaction(async trx => {

		const fromWallet = await Wallet.query(trx).where({ currency: from }).first()
		if (BigNumber(fromWallet.balance).isLessThan(total)) {
			throw new ValidationError(`insufficient balance "${from}"`)
		}
		fromWallet.balance = BigNumber(fromWallet.balance).minus(total).toString()
		await Wallet.query(trx).where({ currency: from }).update(fromWallet)

		const toWallet = await Wallet.query(trx).where({ currency: to }).first()
		toWallet.balance = BigNumber(toWallet.balance).plus(amount).toString()
		await Wallet.query(trx).where({ currency: to }).update(toWallet)

		await Operation.query(trx).insert({
			fromCurrency: from,
			fromAmount: total.toString(),
			fee: fee.toString(),
			toCurrency: to,
			toAmount: amount.toString(),
			time: new Date(),
		})
	})

	return { rate, amount, fee, total }
}
