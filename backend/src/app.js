import express from 'express'
const { Model } = require('objection')

import sourceMapSupport from 'source-map-support'
sourceMapSupport.install()

import knexConfig from '../knexfile'
import api from './api'

export const knex = require('knex')(knexConfig)
Model.knex(knex)

const app = express()

app.use('/api', api)

app.use((error, req, res, next) => { // eslint-disable-line
	console.error(error)
	return res.sendStatus(500)
})

export default app
