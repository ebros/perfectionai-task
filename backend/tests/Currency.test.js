import assert from 'assert'
import { create, assertProps } from './util'
import Currency from '../src/models/Currency'


describe('Currency', () => {

	beforeEach(async function () {
		this.timeout(10000)
		await create.cleanDatabase()
	})

	describe('updateCache()', async () => {

		beforeEach(async () => {
			const fetch = require('node-fetch')
			fetch.fakeAll(async () => {
				return {
					ok: true,
					json: () => Promise.resolve({
						data: {
							quote: {
								VNDC: { price: 1 },
								EUR: { price: 2 },
							},
						},
					}),
				}
			})
		})

		it('should create currency cache', async () => {
			await Currency.updateCache()

			const currencies = await Currency.query().orderBy('symbol', 'asc')
			assert.equal(currencies.length, 3)
			assertProps(currencies[0], { symbol: 'EUR', price: '0.500000000' })
			assertProps(currencies[1], { symbol: 'USD', price: '1.000000000' })
			assertProps(currencies[2], { symbol: 'VNDC', price: '1.000000000' })
		})

		it('should update currency cache', async () => {
			await	create.currency({ symbol: 'VNDC', price: 4 })
			await create.currency({ symbol: 'EUR', price: 4 })
			await create.currency({ symbol: 'USD', price: 4 })

			await Currency.updateCache()

			const currencies = await Currency.query().orderBy('symbol', 'asc')
			assert.equal(currencies.length, 3)
			assertProps(currencies[0], { symbol: 'EUR', price: '0.500000000' })
			assertProps(currencies[1], { symbol: 'USD', price: '1.000000000' })
			assertProps(currencies[2], { symbol: 'VNDC', price: '1.000000000' })
		})
	})

})
