import assert from 'assert'
import { create, assertProps } from './util'
import app from '../src/app'
import request from 'supertest'
import Wallet from '../src/models/Wallet'
import Operation from '../src/models/Operation'


describe('/', () => {

	beforeEach(async function () {
		this.timeout(10000)
		await create.cleanDatabase()
	})

	describe('POST /api/sell', async () => {

		beforeEach(async () => {
			await create.wallet({ currency: 'EUR', balance: '20' })
			await create.wallet({ currency: 'USD', balance: '20' })
			await create.wallet({ currency: 'VNDC', balance: '20' })

			await create.currency({ symbol: 'EUR', price: '1.11111111' })
			await create.currency({ symbol: 'USD', price: '1' })
			await create.currency({ symbol: 'VNDC', price: '0.00004329' })
		})

		it('should convert currecies', async () => {
			await request(app)
				.post('/api/sell')
				.send({ from: 'USD', amount: 10, to: 'EUR' })
				.expect(200, {
					rate: '0.9',
					amount: '9',
					amountCurrency: 'EUR',
					fee: '0.1',
					feeCurrency: 'USD',
					total: '10.1',
					totalCurrency: 'USD',
				})

			const usdWallet = await Wallet.query().where({ currency: 'USD' }).first()
			const eurWallet = await Wallet.query().where({ currency: 'EUR' }).first()
			const operations = await Operation.query()
			assert.equal(usdWallet.balance, '9.900000000')
			assert.equal(eurWallet.balance, '29.000000000')
			assert.equal(operations.length, 1)
			assertProps(operations[0], {
				fromCurrency: 'USD',
				fromAmount: '10.100000000',
				fee: '0.100000000',
				toCurrency: 'EUR',
				toAmount: '9.000000000',
			})
		})

		it('should convert currecies', async () => {
			await request(app)
				.post('/api/sell')
				.send({ from: 'EUR', amount: 10, to: 'VNDC' })
				.expect(200, {
					rate: '25666.69230769',
					amount: '256667',
					amountCurrency: 'VNDC',
					fee: '0.1',
					feeCurrency: 'EUR',
					total: '10.1',
					totalCurrency: 'EUR',
				})

			const eurWallet = await Wallet.query().where({ currency: 'EUR' }).first()
			const vndcWallet = await Wallet.query().where({ currency: 'VNDC' }).first()
			const operations = await Operation.query()
			assert.equal(eurWallet.balance, '9.900000000')
			assert.equal(vndcWallet.balance, '256687.000000000')
			assert.equal(operations.length, 1)
			assertProps(operations[0], {
				fromCurrency: 'EUR',
				fromAmount: '10.100000000',
				fee: '0.100000000',
				toCurrency: 'VNDC',
				toAmount: '256667.000000000',
			})
		})

		it('should return error if insufficient balance', async () => {
			await request(app)
				.post('/api/sell')
				.send({ from: 'USD', amount: 20, to: 'EUR' })
				.expect(400, {
					message: 'ValidationError',
					extra: 'insufficient balance "USD"',
				})
		})

		it('should return error if invalid to currency', async () => {
			await request(app)
				.post('/api/sell')
				.send({ from: 'USD', amount: 20, to: 'RUB' })
				.expect(400, {
					message: 'ValidationError',
					extra: 'Invalid currency "RUB"',
				})
		})

		it('should return error if invalid from currency', async () => {
			await request(app)
				.post('/api/sell')
				.send({ from: 'RUB', amount: 20, to: 'EUR' })
				.expect(400, {
					message: 'ValidationError',
					extra: 'Invalid currency "RUB"',
				})
		})
	})


	describe('POST /api/buy', async () => {

		beforeEach(async () => {
			await create.wallet({ currency: 'EUR', balance: '20' })
			await create.wallet({ currency: 'USD', balance: '20' })

			await create.currency({ symbol: 'EUR', price: '1.1111' })
			await create.currency({ symbol: 'USD', price: '1' })
		})

		it('should convert currecies', async () => {
			await request(app)
				.post('/api/buy')
				.send({ from: 'USD', amount: 10, to: 'EUR' })
				.expect(200, {
					rate: '0.900009',
					amount: '10',
					amountCurrency: 'EUR',
					fee: '0.11',
					feeCurrency: 'USD',
					total: '11.22',
					totalCurrency: 'USD',
				})

			const usdWallet = await Wallet.query().where({ currency: 'USD' }).first()
			const eurWallet = await Wallet.query().where({ currency: 'EUR' }).first()
			const operations = await Operation.query()
			assert.equal(usdWallet.balance, '8.780000000')
			assert.equal(eurWallet.balance, '30.000000000')
			assert.equal(operations.length, 1)
			assertProps(operations[0], {
				fromCurrency: 'USD',
				fromAmount: '11.220000000',
				fee: '0.110000000',
				toCurrency: 'EUR',
				toAmount: '10.000000000',
			})
		})

		it('should return error if insufficient balance', async () => {
			await request(app)
				.post('/api/sell')
				.send({ from: 'USD', amount: 20, to: 'EUR' })
				.expect(400, {
					message: 'ValidationError',
					extra: 'insufficient balance "USD"',
				})
		})
	})


	describe('POST /api/sell-all', async () => {

		beforeEach(async () => {
			await create.wallet({ currency: 'EUR', balance: '10' })
			await create.wallet({ currency: 'USD', balance: '10' })

			await create.currency({ symbol: 'EUR', price: '1.11111111' })
			await create.currency({ symbol: 'USD', price: '1' })
		})

		it('should sell all USD', async () => {
			await request(app)
				.post('/api/sell-all')
				.send({ from: 'USD', to: 'EUR' })
				.expect(200, {
					rate: '0.9',
					amount: '8.91',
					amountCurrency: 'EUR',
					fee: '0.1',
					feeCurrency: 'USD',
					total: '10',
					totalCurrency: 'USD',
				})

			const usdWallet = await Wallet.query().where({ currency: 'USD' }).first()
			const eurWallet = await Wallet.query().where({ currency: 'EUR' }).first()
			const operations = await Operation.query()
			assert.equal(usdWallet.balance, 0)
			assert.equal(eurWallet.balance, 18.91)
			assert.equal(operations.length, 1)
			assertProps(operations[0], {
				fromCurrency: 'USD',
				fromAmount: '10.000000000',
				fee: '0.100000000',
				toCurrency: 'EUR',
				toAmount: '8.910000000',
			})
		})
	})


	describe('GET /api/balances', async () => {

		it('should return all wallet balances', async () => {
			await create.wallet({ currency: 'EUR', balance: '10' })
			await create.wallet({ currency: 'USD', balance: '20' })

			await request(app)
				.get('/api/balances')
				.expect(200, [
					{
						currency: 'EUR',
						balance: '10',
					},
					{
						currency: 'USD',
						balance: '20',
					},
				])
		})
	})


	describe('GET /api/operations', async () => {

		it('should return all operations', async () => {
			const operation = await create.operation({
				fromCurrency: 'USD',
				fromAmount: '1',
				toCurrency: 'EUR',
				toAmount: '2',
				fee: '3',
				time: new Date(),
			})

			await request(app)
				.get('/api/operations')
				.expect(200, [
					{
						fromCurrency: operation.fromCurrency,
						fromAmount: operation.fromAmount,
						toCurrency: operation.toCurrency,
						toAmount: operation.toAmount,
						fee: operation.fee,
						time: operation.time.toISOString(),
					},
				])
		})
	})

})
