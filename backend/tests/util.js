import config from '../config'
import assert from 'assert'


assert(config.mysqlConnectionStringForTests, 'config.mysqlConnectionStringForTests is required to run tests')
config.mysqlConnectionString = config.mysqlConnectionStringForTests
config.cryptoCurrencies = ['VNDC']
config.fiatCurrencies = ['EUR', 'USD']

const { knex } = require('../src/app')

before(async () => {
	console.warn = () => {}

	const [, pending] = await knex.migrate.list()
	if (pending.length > 0) {
		await knex.migrate.latest()
	}
})

beforeEach(() => {
	config.CMCApiKey = 'TEST'

	require('node-fetch')
	require.cache[require.resolve('node-fetch')].exports = stubFn()
})


export function assertProps(actual, expected, message) {
	var keys = Object.keys(expected)
	var actualProps = {}
	if (actual) {
		for (var key of keys) {
			if (actual) {
				actualProps[key] = actual[key]
			}
		}
	}
	else {
		actualProps = actual
	}
	assert.deepStrictEqual(actualProps, expected, message)
}


export function stubFn(returnValue) {
	var fn = function () {
		fn.called++
		var args = Array.prototype.slice.call(arguments)
		fn.args.push(args)

		var fakeFunction = fn.fakeFunctions[fn.called - 1] || fn.fakeAllFn
		if (fakeFunction) {
			return fakeFunction.apply(null, arguments)
		}

		var lastArg = args[args.length - 1]
		if (typeof lastArg === 'function') { lastArg(null) }
		return fn.returnValue
	}

	fn.returnValue = returnValue
	fn.called = 0
	fn.args = []

	fn.fakeFunctions = []
	fn.fake = function (fakeFunction) {
		fn.fakeFunctions.push(fakeFunction)
		return fn
	}
	fn.fakeAll = function (fakeFunction) {
		fn.fakeAllFn = fakeFunction
		return fn
	}

	return fn
}


export { default as create } from './create'
