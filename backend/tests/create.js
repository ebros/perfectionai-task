import Currency from '../src/models/Currency'
import Operation from '../src/models/Operation'
import Wallet from '../src/models/Wallet'
import config from '../config'


async function cleanDatabase() {

	if (!config.mysqlConnectionString.includes('testing')) {
		throw new Error(`Will not delete data from non testing DB '${config.mysqlConnectionString}'. Configure dbStringForTests`)
	}

	await Promise.all([
		Currency.query().delete(),
		Operation.query().delete(),
		Wallet.query().delete(),
	])
}


async function currency(data = {}) {
	data = Object.assign({}, data)
	return await Currency.query().insert(data)
}


async function operation(data = {}) {
	data = Object.assign({}, data)
	data.time.setMilliseconds(0)
	return await Operation.query().insert(data)
}


async function wallet(data = {}) {
	data = Object.assign({}, data)
	return await Wallet.query().insert(data)
}


export default {
	cleanDatabase,
	currency,
	operation,
	wallet,
}
