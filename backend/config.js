require('dotenv').config({ path: '../.env' })

module.exports = {
	mysqlConnectionString: process.env.MYSQL_CONNECTION_STRING || 'mysql://root:password@localhost/perfectionai-task',
	mysqlConnectionStringForTests: process.env.MYSQL_CONNECTION_STRING_FOR_TESTS || 'mysql://root:password@localhost:3307/perfectionai-testing',
	httpPort: process.env.HTTP_PORT || 8080,
	cryptoCurrencies: 'BTC ETH VNDC'.split(' '),
	fiatCurrencies: 'USD EUR'.split(' '),
	CMCApiKey: process.env.CMC_API_KEY || 'b8f5c003-d5d8-42df-864e-33e745ceeeee',
	conversionFee: process.env.CONVERSION_FEE || 0.01,
}
