# PerfectionAI backend task

Project uses Node.js and MySQL. Either start one with Docker `docker run --rm -p 3306:3306 -e MYSQL_DATABASE=perfectionai-task -e MYSQL_ROOT_PASSWORD=password --name mysql mysql:5`. Or point to existing one using environment variables MYSQL_CONNECTION_STRING and MYSQL_CONNECTION_STRING_FOR_TESTS. Environment variables can also be set using .env file

## Development
```
npm install
npm start
```

# Tests
```
docker run --rm -p 3307:3306 -e MYSQL_DATABASE=perfectionai-testing -e MYSQL_ROOT_PASSWORD=password --name mysqlt mysql:5
npm install
npm test
```

# Build and run
```
docker-compose build
docker-compose up
```
