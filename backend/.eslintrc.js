module.exports = {
	env: {
		es6: true,
		node: true,
	},
	parser: 'babel-eslint',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
	},
	extends: [
		'eslint:recommended',
		'plugin:import/errors',
	],
	rules: {
		'indent': ['error', 'tab', { 'SwitchCase': 1 }],
		'quotes': ['error', 'single'],
		'semi': ['error', 'never'],
		'key-spacing': ['error', { 'beforeColon': false, 'afterColon': true }],
		'no-trailing-spaces': 'error',
		'space-before-blocks': 'error',
		'space-before-function-paren': ['error', { anonymous: 'always', named: 'ignore' }],
		'keyword-spacing': ['error', { 'before': true }],
		'space-in-parens': ['error', 'never'],
		'object-curly-spacing': ['error', 'always'],
		'no-console': ['error', { allow: ['warn', 'error', 'info'] }],
		'comma-dangle': ['error', 'always-multiline'],
		'eol-last': ['error'],
		'require-atomic-updates': 0,
	},
}
