import React, { useState, useEffect, useRef } from 'react'
import Card from './Card'
import config from '../../configuration'
import { makeSearch, useLocalStorage } from '../util'


function FlickerPhotoList() {
	const [photos, setPhotos] = useState([])
	const ListBottom = useRef(null)
	const [loading, setLoading] = useState(false)
	const [needsLoading, setNeedsLoading] = useState(true)
	const [favorites, setFavorites] = useLocalStorage('favorites', [])

	function toggleFavorite(id) {
		setFavorites(x => {
			return x.includes(id)
				? x.filter(p => p !== id)
				: [...x, id]
		})
	}

	useEffect(() => {
		if (!needsLoading) { return }
		if (loading) { return }

		setLoading(true)
		;(async () => {
			const params = {
				method: 'flickr.photos.getRecent',
				api_key: config.apiKey,
				format: 'json',
				nojsoncallback: 1,
				per_page: 12,
				page: photos.length / 12 + 1,
				extras: 'date_upload,owner_name',
			}
			const url = 'https://api.flickr.com/services/rest' + makeSearch(params)

			const json = await (await fetch(url)).json()
			setPhotos(photos => photos.concat(json.photos.photo))
			setNeedsLoading(false)
			setLoading(false)
		})()

	}, [loading, photos, needsLoading])

	useEffect(() => {
		setNeedsLoading(false)
		const observer = new IntersectionObserver(
			([entry]) => {
				if (entry.isIntersecting) {
					setNeedsLoading(true)
				}
			}, { rootMargin: '600px' },
		)
		if (ListBottom.current) {
			observer.observe(ListBottom.current)
		}
		return () => observer.disconnect()
	}, [ListBottom, loading])

	return (<>
		<ul className="o-card-grid">
			{ photos.map((p, i) => (
				<Card key={i} title={p.title} author={p.ownername}  imageAlt={p.title}
					image={`https://farm${p.farm}.staticflickr.com/${p.server}/${p.id}_${p.secret}.jpg`}
					isActive={favorites.includes(p.id)}
					onClick={() => toggleFavorite(p.id)}
				/>
			))}
		</ul>
		<div ref={ListBottom}></div>
	</>)
}

FlickerPhotoList.propTypes = {}

export default FlickerPhotoList
