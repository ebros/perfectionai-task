import React from 'react'
import PropTypes from 'prop-types'


function Card(props) {
	return <li className="m-card">
		<img src={props.image} alt={props.imageAlt} width="300" height="225" loading="lazy" />
		{(props.title || props.author) && <div className="m-card__text-group">
			{props.title && <strong>{props.title}</strong>}
			{props.title && props.author && <div className="m-card__text-group-divider"></div>}
			{props.author && <em>{props.author}</em>}
			{props.onClick &&
				<button type="button"
					className={'btn ' + (props.isActive && 'active')}
					onClick={props.onClick}
				>
					Favourite
				</button>}
		</div>}
	</li>
}

Card.propTypes = {
	image: PropTypes.string.isRequired,
	imageAlt: PropTypes.string,
	title: PropTypes.string,
	author: PropTypes.string,
	onClick: PropTypes.func,
	isActive: PropTypes.bool,
}

export default Card
