import React from 'react'
import ReactDOM from 'react-dom'
import '@babel/polyfill'

import './style/main.scss'
import FlickerPhotoList from './components/FlickerPhotoList'

const jsx = (
	<FlickerPhotoList />
)

const app = document.getElementById('app')
ReactDOM.render(jsx, app)
