import { useState, useEffect } from 'react'

export function makeSearch(options = {}) {
	const query = Object.keys(options)
		.filter(x => options[x] != null && options[x] !== '' && (!Array.isArray(options[x]) || options[x].length > 0))
		.map(x => `${x}=${encodeURIComponent(options[x])}`)
		.join('&')

	return query ? '?' + query : ''
}


export function useLocalStorage(key, initialValue) {
	const [value, setValue] = useState(() => {
		try {
			const item = localStorage.getItem(key)
			return item ? JSON.parse(item) : initialValue
		}
		catch (e) {
			return initialValue
		}
	})

	useEffect(() => {
		localStorage.setItem(key, JSON.stringify(value))
	}, [value])

	return [value, setValue]
}
