# PerfectionAI backend task

Project uses Node.js

## Development
```
npm install
npm start
```

# Build and run
```
docker-compose build
docker-compose up
```
