const dev = process.env.NODE_ENV !== 'production'
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const TerserJSPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')


const config = {
	target: 'web',
	mode: dev ? 'development' : 'production',
	devtool: dev ? 'cheap-source-map' : 'hidden-source-map',
	entry: {
		index: './src/index.js',
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.js?v=[chunkhash]',
		publicPath: '/',
	},
	stats: 'minimal',
	resolve: {
		modules: [
			path.resolve('./src'),
			'node_modules',
		],
	},

	devServer: {
		compress: true,
		contentBase: path.join(__dirname, 'dist'),
		historyApiFallback: { index: '/' },
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env', '@babel/preset-react'],
					plugins: [
						'@babel/plugin-transform-runtime',
					],
				},
			},
			{
				test: /\.scss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					'css-loader',
					'sass-loader',
				],
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					'css-loader',
				],
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].bundle.css?v=[chunkhash]',
		}),
		new CopyWebpackPlugin({
			patterns: [
				{ from: './src/public', force: true },
			],
		}),
		new HtmlWebpackPlugin({
			template: './src/index.html',
			filename: 'index.html',
		}),
	],
}

if (!dev) {
	config.optimization = {
		minimizer: [
			new TerserJSPlugin({ cache: true, parallel: true, sourceMap: true }),
			new OptimizeCSSAssetsPlugin({}),
		],
	}
}


module.exports = config
